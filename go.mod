module gitlab.com/hello-world4/shippy-service-vessel

go 1.15

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

require (
	github.com/golang/protobuf v1.4.3
	github.com/micro/go-micro/v2 v2.9.1
	go.mongodb.org/mongo-driver v1.4.4
	google.golang.org/protobuf v1.25.0
)
