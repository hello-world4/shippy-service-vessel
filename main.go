package main

import (
	"context"
	"log"
	"os"

	pb "gitlab.com/hello-world4/shippy-service-vessel/proto/vessel"
	"github.com/micro/go-micro/v2"
)

func main() {
	service := micro.NewService(
		micro.Name("shippy.service.vessel"),
	)

	service.Init()

	uri := os.Getenv("DB_HOST")

	client, err := CreateClient(context.Background(), uri, 0)
	if err != nil {
		log.Panic(err)
	}
	defer client.Disconnect(context.Background())

	vesselCollection := client.Database("shippy").Collection("vessels")
	repository := &MongoRepository{vesselCollection}
	log.Println("creating vessel")
	if err := repository.Create(context.Background(), &Vessel{
		ID: "vesselID",
		Capacity: 1000,
		Name: "vessel name",
		Available: true,
		OwnerID: "4",
		MaxWeight: 1000,
	}); err != nil {
		log.Println("cant create vessel: ", err)
	}

	log.Println("finding vessel")
	vessel, err := repository.FindAvailable(context.Background(), &Specification{Capacity: 10, MaxWeight: 20})
	if err != nil {
		log.Println("cant find available")
	}
	log.Println("found vessel: ", vessel)
	
	h := &handler{repository}

	// Register our implementation with
	if err := pb.RegisterVesselServiceHandler(service.Server(), h); err != nil {
		log.Panic(err)
	}

	if err := service.Run(); err != nil {
		log.Panic(err)
	}
}